$(function(){

    // Listagem
    function renderConse(){
        $('#linkAdd').html('');
        link = `<a href="?page=cadConse" class="text-left text-dark h6"><i class="bi bi-plus-circle-fill"></i> Adicionar Conselho</a>`
        $('#renderConse').html('');
        
        $.template("loadConse", tempConse);

        $.ajax({
            url : 'http://localhost/projetoyii-leticia/web/index.php?r=api/conselhos/get-all',
            method: 'GET',
            dataType: 'json',
            success: function(data){
                if (data.resultSet) {
                    $.tmlp("loadConse", data.resultSet).append("#renderConse");
                }
                $('.loader').remove();
                $('#linkAdd').html(link);
            }
        })
    }

    renderConse();

    // Abrir edição de registro
    $(document).on('click', 'a[href="#"].editar', function(){
        idRegistro = $(this).attr('data-id');
        link = $(this);
        $.ajax({
            url : 'http://localhost/projetoyii-leticia/web/index.php?r=api/conselhos/get-one&id='+idRegistro,
            method: 'GET',
            dataType: 'json',
            success: function(data){
                condominio = data.resultSet[0].nomeCondominio;
                from_condominio = data.resultSet[0].from_condominio;
                loadCond(from_condominio);
                $(link).parent().parent().find('.from_condominio').html(`<select name="from_condominio" class="fromCond"></select>`);
                
                nome = data.resultSet[0].nome;
                $(link).parent().parent().find('.nome').html(`<input type="text" name="nome" value="${nome}">`);

                from_funcao = data.resultSet[0].from_funcao;
                $(link).parent().parent().find('.from_funcao').html(`
                    <select class="from_funcao" name="from_funcao" required>
                        <option value="" disabled selected>Selecione a Função</option>
                        <option value="Subsíndico" ${`('Subsíndico' == ${from_funcao} ? 'selected' : '')`} >Subsíndico(a)</option>
                        <option value="Conselheiro" ${`('Conselheiro' == ${from_funcao} ? 'selected' : '')`} >Conselheiro(a)</option>
                    </select>`);
                
            }
        })
        // Mudar botões de editar/remover para salvar/cancelar
        $(link).parent().find('.editar').toggleClass(['editar', 'salvar']);
        $(link).html('<i class="bi bi-check-circle-fill text-dark h4"></i>');
        
        $(link).parent().find('.remover').html('<i class="bi bi-x-circle text-dark h4"></i>');
        $(link).parent().find('.remover').toggleClass(['remover', 'cancelar']);

        $('.editar, .remover').removeAttr('href');
    })
    
    // Cancelar edição
    $(document).on('click', '.cancelar', function(){
        $(this).parent().parent().find('.from_condominio').html(condominio);
        $(this).parent().parent().find('.nome').html(nome);
        $(this).parent().parent().find('.from_funcao').html(from_funcao);
    
        // Criar botões
        $(this).parent().find('.salvar').html('<i class="bi bi-pencil-square text-dark h4"></i>');
        $(this).parent().find('.salvar').toggleClass(['salvar', 'editar']);
    
        $(this).parent().find('.cancelar').html('<i class="bi bi-trash3 text-dark h4"></i>');
        $(this).parent().find('.cancelar').toggleClass(['cancelar', 'remover']);

        $('.editar, .remover').attr('href','#');
    })


    // Editar registro
    $(document).on('click','.salvar',function(){
        
        id = $(this).attr('data-id')
        from_condominio = $('select[name="from_condominio"]').val();
        nome = $('input[name="nome"]').val();
        from_funcao = $('select[name="from_funcao"]').val();

        dados = `from_condominio=${from_condominio}&nome=${nome}&from_funcao=${from_funcao}&id=${id}`;

        if (from_condominio, nome, from_funcao) {
            retorno = postApi('http://localhost/projetoyii-leticia/web/index.php?r=api/conselhos/edit-conse', dados);
        }
    })


    // Deletar Registro
    $(document).on('click', '.remover', function(){
        idRegistro = $(this).attr('data-id');
        postApi('http://localhost/projetoyii-leticia/web/index.php?r=api/conselhos/delete', `&id=${idRegistro}`);
    })
})