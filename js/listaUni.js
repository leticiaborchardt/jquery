$(function(){

    // Listagem
    function renderUni(){
        $('#linkAdd').html('');
        link = `<a href="?page=cadUni" class="text-left text-dark h6"><i class="bi bi-plus-circle-fill"></i> Adicionar Unidade</a>`
        $('#renderUni').html('');
        
        $.template("loadUni", tempUni)

        $.ajax({
            url : 'http://localhost/projetoyii-leticia/web/index.php?r=api/unidades/get-all',
            method: 'GET',
            dataType: 'json',
            success: function(data){
                if (data.resultSet) {
                    $.tmpl("loadUni", data.resultSet).append("#renderUni");
                }
                $('.loader').remove();
                $('#linkAdd').html(link);
            }
        })
    }

    renderUni();

    // Abrir edição de registro
    $(document).on('click', 'a[href="#"].editar', function(){
        idRegistro = $(this).attr('data-id');
        link = $(this);
        $.ajax({
            url : 'http://localhost/projetoyii-leticia/web/index.php?r=api/unidades/get-one&id='+idRegistro,
            method: 'GET',
            dataType: 'json',
            success: function(data){
                condominio = data.resultSet[0].nomeCondominio;
                from_condominio = data.resultSet[0].from_condominio;
                loadCond(from_condominio);
                $(link).parent().parent().find('.from_condominio').html(`<select name="from_condominio" class="fromCond"></select>`);
                
                nomeBloco = data.resultSet[0].nomeBloco;
                $(link).parent().parent().find('.nomeBloco').html(`<input type="text" name="nomeBloco" value="${nomeBloco}">`);
                nomeUnidade = data.resultSet[0].nomeUnidade;
                $(link).parent().parent().find('.nomeUnidade').html(`<input type="text" name="nomeUnidade" value="${nomeUnidade}">`);
                metragem = data.resultSet[0].metragem;
                $(link).parent().parent().find('.metragem').html(`<input type="text" name="metragem" value="${metragem}">`);
                qtdGaragem = data.resultSet[0].qtdGaragem;
                $(link).parent().parent().find('.qtdGaragem').html(`<input type="text" name="qtdGaragem" value="${qtdGaragem}">`);
            
            }
        })
        // Mudar botões de editar/remover para salvar/cancelar
        $(link).parent().find('.editar').toggleClass(['editar', 'salvar']);
        $(link).html('<i class="bi bi-check-circle-fill text-dark h4"></i>');
        
        $(link).parent().find('.remover').html('<i class="bi bi-x-circle text-dark h4"></i>');
        $(link).parent().find('.remover').toggleClass(['remover', 'cancelar']);

        $('.editar, .remover').removeAttr('href');
    })
    
    // Cancelar edição
    $(document).on('click', '.cancelar', function(){
        $(this).parent().parent().find('.from_condominio').html(condominio);
        $(this).parent().parent().find('.nomeBloco').html(nomeBloco);
        $(this).parent().parent().find('.nomeUnidade').html(nomeUnidade);
        $(this).parent().parent().find('.metragem').html(metragem);
        $(this).parent().parent().find('.qtdGaragem').html(qtdGaragem);
    
        // Criar botões
        $(this).parent().find('.salvar').html('<i class="bi bi-pencil-square text-dark h4"></i>');
        $(this).parent().find('.salvar').toggleClass(['salvar', 'editar']);
    
        $(this).parent().find('.cancelar').html('<i class="bi bi-trash3 text-dark h4"></i>');
        $(this).parent().find('.cancelar').toggleClass(['cancelar', 'remover']);

        $('.editar, .remover').attr('href','#');
    })


    // Editar registro
    $(document).on('click','.salvar',function(){
        
        id = $(this).attr('data-id')
        from_condominio = $('select[name="from_condominio"]').val();
        nomeBloco = $('input[name="nomeBloco"]').val();
        nomeUnidade = $('input[name="nomeUnidade"]').val();
        metragem = $('input[name="metragem"]').val();
        qtdGaragem = $('input[name="qtdGaragem"]').val();

        dados = `from_condominio=${from_condominio}&nomeBloco=${nomeBloco}&nomeUnidade=${nomeUnidade}&metragem=${metragem}&qtdGaragem=${qtdGaragem}&id=${id}`;

        if (nomeCondominio, nomeBloco, nomeUnidade, metragem, qtdGaragem) {
            retorno = postApi('http://localhost/projetoyii-leticia/web/index.php?r=api/unidades/edit-uni', dados);
        }
    })


    // Deletar Registro
    $(document).on('click', '.remover', function(){
        idRegistro = $(this).attr('data-id');
        postApi('http://localhost/projetoyii-leticia/web/index.php?r=api/unidades/delete', `&id=${idRegistro}`);
    })
})