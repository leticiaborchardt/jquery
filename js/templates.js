// --------------------- ADMINISTRADORAS
let tempAdm = '';
tempAdm =+ '<tr data-id="${id}">';
    tempAdm =+ '<td class="nome">${nome}</td>';
    tempAdm =+ '<td class="cnpj">${cnpj}</td>';
    tempAdm =+ '<td>${dataCadastro}</td>';
    tempAdm =+ '<td>';
        tempAdm =+ '<a href="#" data-id="${id}" class="editar"><i class="bi bi-pencil-square text-dark h4"></i></a>';
        tempAdm =+ '<a href="#" data-id="${id}" class="remover"><i class="bi bi-trash3 text-dark h4"></i></a>';
    tempAdm =+ '</td>';
tempAdm =+ '</tr>`';


// --------------------- CONDOMÍNIOS
let tempCondo = '';
tempCondo += '<tr data-id="${id}">';
    tempCondo += '<input type="hidden" name="id" value="${id}">';
    tempCondo += '<td class="from_administradora">${nomeAdm}</td>';
    tempCondo += '<td class="nomeCondominio">${nomeCondominio}</td>';
    tempCondo += '<td class="qtdBlocos">${qtdBlocos}</td>';
    tempCondo += '<td class="lougradouro">${lougradouro}</td>';
    tempCondo += '<td class="numero">${numero}</td>';
    tempCondo += '<td class="bairro">${bairro}</td>';
    tempCondo += '<td class="cidade">${cidade}</td>';
    tempCondo += '<td class="estado">${estado}</td>';
    tempCondo += '<td class="cep">${cep}</td>';
    tempCondo += '<td class="sindico">${sindico}</td>';
    tempCondo += '<td>${dataCadastro}</td>';
    tempCondo += '<td>';
        tempCondo += '<a href="#" data-id="${id}" class="editar"><i class="bi bi-pencil-square text-dark h4"></i></a>';
        tempCondo += '<a href="#" data-id="${id}" class="remover"><i class="bi bi-trash3 text-dark h4"></i></a>';
    tempCondo += '</td>';
tempCondo += '</tr>';


// --------------------- BLOCOS
let tempBloco = '';
tempBloco += '<tr data-id="${id}">';
    tempBloco += '<td class="from_condominio">${nomeCondominio}</td>';
    tempBloco += '<td class="nomeBloco">${nomeBloco}</td>';
    tempBloco += '<td class="andares">${andares}</td>';
    tempBloco += '<td class="qtdUnidades">${qtdUnidades}</td>';
    tempBloco += '<td>${dataCadastro}</td>';
    tempBloco += '<td>';
        tempBloco += '<a href="#" data-id="${id}" class="editar"><i class="bi bi-pencil-square text-dark h4"></i></a>';
        tempBloco += '<a href="#" data-id="${id}" class="remover"><i class="bi bi-trash3 text-dark h4"></i></a>';
    tempBloco += '</td>';
tempBloco += '</tr>`;';


// --------------------- UNIDADES
let tempUni = '';
tempUni += '<tr data-id="${id}">';
    tempUni += '<td class="from_condominio">${nomeCondominio}</td>';
    tempUni += '<td class="nomeBloco">${nomeBloco}</td>';
    tempUni += '<td class="nomeUnidade">${nomeUnidade}</td>';
    tempUni += '<td class="metragem">${metragem}</td>';
    tempUni += '<td class="qtdGaragem">${qtdGaragem}</td>';
    tempUni += '<td>${dataCadastro}</td>';
    tempUni += '<td>';
        tempUni += '<a href="#" data-id="${id}" class="editar"><i class="bi bi-pencil-square text-dark h4"></i></a>';
        tempUni += '<a href="#" data-id="${id}" class="remover"><i class="bi bi-trash3 text-dark h4"></i></a>';
    tempUni += '</td>';
tempUni += '</tr>`';


// --------------------- MORADORES
let tempMor = '';
tempMor += '<tr data-id="${id}">';
    tempMor += '<td class="nomeCondominio">${nomeCondominio}</td>';
    tempMor += '<td class="nomeBloco">${nomeBloco}</td>';
    tempMor += '<td class="nomeUnidade">${nomeUnidade}</td>';
    tempMor += '<td class="nome">${nome}</td>';
    tempMor += '<td class="cpf">${cpf}</td>';
    tempMor += '<td class="email">${email}</td>';
    tempMor += '<td class="telefone">${telefone}</td>';
    tempMor += '<td>${dataCadastro}</td>';
    tempMor += '<td>';
        tempMor += '<a href="#" data-id="${id}" class="editar"><i class="bi bi-pencil-square text-dark h4"></i></a>';
        tempMor += '<a href="#" data-id="${id}" class="remover"><i class="bi bi-trash3 text-dark h4"></i></a>';
    tempMor += '</td>';
tempMor += '</tr>';


// --------------------- CONSELHOS
let tempConse = '';
tempConse += '<tr data-id="${id}">';
    tempConse += '<td class="from_condominio">${nomeCondominio}</td>';
    tempConse += '<td class="nome">${nome}</td>';
    tempConse += '<td class="from_funcao">${from_funcao}</td>';
    tempConse += '<td>${dataCadastro}</td>';
    tempConse += '<td>';
        tempConse += '<a href="#" data-id="${id}" class="editar"><i class="bi bi-pencil-square text-dark h4"></i></a>';
        tempConse += '<a href="#" data-id="${id}" class="remover"><i class="bi bi-trash3 text-dark h4"></i></a>';
    tempConse += '</td>';
tempConse += '</tr>';