$(function(){

    // Listagem
    function renderAdm(){
        $('#linkAdd').html('');
        link = `<a href="?page=cadAdm" class="text-left text-dark h6"><i class="bi bi-plus-circle-fill"></i> Adicionar Administradora</a>`
        $('#renderAdm').html('');
        
        $.template("loadAdm", tempAdm),

        $.ajax({
            url : 'http://localhost/projetoyii-leticia/web/index.php?r=api/administradoras/get-all',
            method: 'GET',
            dataType: 'json',
            success: function(data){
                if (data.resultSet) {
                    $.tmpl("loadAdm", data.resultSet).append("#renderAdm")
                }
                $('.loader').remove();
                $('#linkAdd').html(link);
            }
        })
    }

    renderAdm();

    // Abrir edição de registro
    $(document).on('click', 'a[href="#"].editar', function(){
        idRegistro = $(this).attr('data-id');
        link = $(this);
        $.ajax({
            url : 'http://localhost/projetoyii-leticia/web/index.php?r=api/administradoras/get-one&id='+idRegistro,
            method: 'GET',
            dataType: 'json',
            success: function(data){
                nome = data.resultSet[0].nome;
                $(link).parent().parent().find('.nome').html(`<input type="text" name="nome" value="${nome}">`);
                cnpj = data.resultSet[0].cnpj;
                $(link).parent().parent().find('.cnpj').html(`<input type="text" name="cnpj" value="${cnpj}">`);

            }
        })
        // Mudar botões de editar/remover para salvar/cancelar
        $(link).parent().find('.editar').toggleClass(['editar', 'salvar']);
        $(link).html('<i class="bi bi-check-circle-fill text-dark h4"></i>');
        
        $(link).parent().find('.remover').html('<i class="bi bi-x-circle text-dark h4"></i>');
        $(link).parent().find('.remover').toggleClass(['remover', 'cancelar']);

        $('.editar, .remover').removeAttr('href');
    })
    
    // Cancelar edição
    $(document).on('click', '.cancelar', function(){
        $(this).parent().parent().find('.nome').html(nome);
        $(this).parent().parent().find('.cnpj').html(cnpj);
    
        // Criar botões
        $(this).parent().find('.salvar').html('<i class="bi bi-pencil-square text-dark h4"></i>');
        $(this).parent().find('.salvar').toggleClass(['salvar', 'editar']);
    
        $(this).parent().find('.cancelar').html('<i class="bi bi-trash3 text-dark h4"></i>');
        $(this).parent().find('.cancelar').toggleClass(['cancelar', 'remover']);

        $('.editar, .remover').attr('href','#');
    })


    // Editar registro
    $(document).on('click','.salvar',function(){
        
        id = $(this).attr('data-id')
        nome = $('input[name="nome"]').val();
        cnpj = $('input[name="cnpj"]').val();

        dados = `nome=${nome}&cnpj=${cnpj}&id=${id}`;

        if (nome && cnpj) {
            retorno = postApi('http://localhost/projetoyii-leticia/web/index.php?r=api/administradoras/edit-adm', dados);
        }
    })


    // Deletar Registro
    $(document).on('click', '.remover', function(){
        idRegistro = $(this).attr('data-id');
        postApi('http://localhost/projetoyii-leticia/web/index.php?r=api/administradoras/delete', `&id=${idRegistro}`);
    })
})