$(function(){

    // Listagem
    function renderMor(){
        $('#linkAdd').html('');
        link = `<a href="?page=cadMor" class="text-left text-dark h6"><i class="bi bi-plus-circle-fill"></i> Adicionar Morador</a>`
        $('#renderMor').html('');
        
        $.template("loadMor", tempMor);

        $.ajax({
            url : 'http://localhost/projetoyii-leticia/web/index.php?r=api/moradores/get-all',
            method: 'GET',
            dataType: 'json',
            success: function(data){
                if (data.resultSet) {
                    $.tmpl("loadMor", data.resultSet).append("#renderMor")
                }
                $('.loader').remove();
                $('#linkAdd').html(link);
            }
        })
    }

    renderMor();

    // Abrir edição de registro
    $(document).on('click', 'a[href="#"].editar', function(){
        idRegistro = $(this).attr('data-id');
        link = $(this);
        $.ajax({
            url : 'http://localhost/projetoyii-leticia/web/index.php?r=api/moradores/get-one&id='+idRegistro,
            method: 'GET',
            dataType: 'json',
            success: function(data){
                nomeCondominio = data.resultSet[0].nomeCondominio;
                $(link).parent().parent().find('.nomeCondominio').html(`<input type="text" name="nomeCondominio" value="${nomeCondominio}">`);
                nomeBloco = data.resultSet[0].nomeBloco;
                $(link).parent().parent().find('.nomeBloco').html(`<input type="text" name="nomeBloco" value="${nomeBloco}">`);
                nomeUnidade = data.resultSet[0].nomeUnidade;
                $(link).parent().parent().find('.nomeUnidade').html(`<input type="text" name="nomeUnidade" value="${nomeUnidade}">`);
                nome = data.resultSet[0].nome;
                $(link).parent().parent().find('.nome').html(`<input type="text" name="nome" value="${nome}">`);
                cpf = data.resultSet[0].cpf;
                $(link).parent().parent().find('.cpf').html(`<input type="text" name="cpf" value="${cpf}">`);
                email = data.resultSet[0].email;
                $(link).parent().parent().find('.email').html(`<input type="text" name="email" value="${email}">`);
                telefone = data.resultSet[0].telefone;
                $(link).parent().parent().find('.telefone').html(`<input type="text" name="telefone" value="${telefone}">`);
            
            }
        })
        // Mudar botões de editar/remover para salvar/cancelar
        $(link).parent().find('.editar').toggleClass(['editar', 'salvar']);
        $(link).html('<i class="bi bi-check-circle-fill text-dark h4"></i>');
        
        $(link).parent().find('.remover').html('<i class="bi bi-x-circle text-dark h4"></i>');
        $(link).parent().find('.remover').toggleClass(['remover', 'cancelar']);

        $('.editar, .remover').removeAttr('href');
    })
    
    // Cancelar edição
    $(document).on('click', '.cancelar', function(){
        $(this).parent().parent().find('.nomeCondominio').html(nomeCondominio);
        $(this).parent().parent().find('.nomeBloco').html(nomeBloco);
        $(this).parent().parent().find('.nomeUnidade').html(nomeUnidade);
        $(this).parent().parent().find('.nome').html(nome);
        $(this).parent().parent().find('.cpf').html(cpf);
        $(this).parent().parent().find('.email').html(email);
        $(this).parent().parent().find('.telefone').html(telefone);
    
        // Criar botões
        $(this).parent().find('.salvar').html('<i class="bi bi-pencil-square text-dark h4"></i>');
        $(this).parent().find('.salvar').toggleClass(['salvar', 'editar']);
    
        $(this).parent().find('.cancelar').html('<i class="bi bi-trash3 text-dark h4"></i>');
        $(this).parent().find('.cancelar').toggleClass(['cancelar', 'remover']);

        $('.editar, .remover').attr('href','#');
    })


    // Editar registro
    $(document).on('click','.salvar',function(){
        
        id = $(this).attr('data-id')
        nomeCondominio = $('input[name="nomeCondominio"]').val();
        nomeBloco = $('input[name="nomeBloco"]').val();
        nomeUnidade = $('input[name="nomeUnidade"]').val();
        nome = $('input[name="nome"]').val();
        cpf = $('input[name="cpf"]').val();
        email = $('input[name="email"]').val();
        telefone = $('input[name="telefone"]').val();

        dados = `nomeCondominio=${nomeCondominio}&nomeBloco=${nomeBloco}&nomeUnidade=${nomeUnidade}&nome=${nome}&cpf=${cpf}&email=${email}&telefone=${telefone}&id=${id}`;

        if (nomeCondominio, nomeBloco, nomeUnidade, nome, cpf, email, telefone) {
            retorno = postApi('http://localhost/projetoyii-leticia/web/index.php?r=api/moradores/edit-mor', dados);
        }
    })


    // Deletar Registro
    $(document).on('click', '.remover', function(){
        idRegistro = $(this).attr('data-id');
        postApi('http://localhost/projetoyii-leticia/web/index.php?r=api/moradores/delete', `&id=${idRegistro}`);
    })
})