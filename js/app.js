$(function(){

    // Realiza todos os cadastros
    $('form').submit(function(){
        url = $(this).attr('action');
        data = $(this).serialize();
        postApi(url, data);
        return false;
    });

    // Enviar post para qualquer tabela
    postApi = function(url, dados){ // 'dados' são os dados preenchidos no formulário
        $.ajax({
            url : 'http://localhost/projetoyii-leticia/web/index.php?r=api/default/get-token-post',
            method: 'GET',
            dataType: 'json',
            success: function(tk){
                
                $.ajax({
                    url: url,
                    method: 'POST',
                    dataType: 'json',
                    data : dados + '&_csrf='+tk.token,
                    success: function(data){
                        if (data.endPoint.status == 'success') {
                            location.reload();
                        }
                    }
                })
            }
        })
    }

    // Select ADMINISTRADORAS
    loadAdm = function(selected){
        let idSelecionado = (selected) ? selected : null;
        let options = '<option value="">Selecione a Administradora</option>';
        $.ajax({
            url: 'http://localhost/projetoyii-leticia/web/index.php?r=api/administradoras/get-all',
            method: 'GET',
            dataType: 'json',
            success: function(data){
                let dados = data.resultSet;
                for (let key in dados) {
                    options += `<option value="${dados[key].id}" ${(dados[key].id == idSelecionado ? 'selected' : '')}>${dados[key].nome}</option>`;
                }
                $('.fromAdm').html(options);
            }
        })
    }

    $('.fromAdm').ready(function(){
        loadAdm();
    })
    //---------------------------------------------------------------------------------------------------
    
    // Select CONDOMINIOS
    loadCond = function(selected){
        let idSelecionado = (selected) ? selected : null;
        let options = '<option value="">Selecione o Condomínio</option>';
        $.ajax({
            url: 'http://localhost/projetoyii-leticia/web/index.php?r=api/condominios/get-all',
            method: 'GET',
            dataType: 'json',
            success: function(data){
                let dados = data.resultSet;
                for (let key in dados) {
                    options += `<option value="${dados[key].id}" ${(dados[key].id == idSelecionado ? 'selected' : '')}>${dados[key].nomeCondominio}</option>`;
                }
                $('.fromCond').html(options);
            }
        })
    }
    
    $('.fromCond').ready(function(){
        loadCond();
    })
    
    // Cria o select das CIDADES após selecionar o ESTADO - api do ibge
    $('.fromEstado').change(function(){
        let options = '<option value="">Selecione a cidade...</option>';
        let estado = $(this).val();
        let estadoSelecionado = $(this).find(`option[value="${estado}"]`).attr('data-uf');
        $.ajax({
            url: `http://servicodados.ibge.gov.br/api/v1/localidades/estados/${estadoSelecionado}/municipios`,
            method: 'GET',
            dataType: 'json',
            success: function(data) {
                for (let key in data) {
                    options += `<option value="${data[key].nome}">${data[key].nome}</option>`
                }
                $('.cidades').html(options);
            }
        })
    })
    //---------------------------------------------------------------------------------------------------

    // No cadastro da UNIDADE -> Cria o select dos BLOCOS após selecionar o CONDOMINIO
    $('.fromCond').change(function(){
        let options = `'<option value="">Selecione o Bloco</option>'`;
        let id = $(this).val();
        $.ajax({
            url: `http://localhost/projetoyii-leticia/web/index.php?r=api/unidades/get-bloco-from-cond&from_condominio=${id}`,
            method: 'GET',
            dataType: 'json',
            success: function(data){
                let dados = data.resultSet;
                for (let key in dados) {
                    options += `<option value="${dados[key].id}">${dados[key].nomeBloco}</option>`;
                }
                $('.fromBloco').html(options);
            }
        })
    })

    // No cadastro do MORADOR -> Cria o select das UNIDADES após selecionar o BLOCO
    $('.fromBloco').change(function(){
        let options = `'<option value="">Selecione a Unidade</option>'`;
        let id = $(this).val();
        $.ajax({
            url: `http://localhost/projetoyii-leticia/web/index.php?r=api/moradores/get-uni-from-bloco&from_bloco=${id}`,
            method: 'GET',
            dataType: 'json',
            success: function(data){
                let dados = data.resultSet;
                for (let key in dados) {
                    options += `<option value="${dados[key].id}">${dados[key].nomeUnidade}</option>`;
                }
                $('.fromUni').html(options);
            }
        })
    })

});
    
    