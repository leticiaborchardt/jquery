$(function(){

    // Listagem
    function renderBloco(){
        $('#linkAdd').html('');
        link = `<a href="?page=cadBloco" class="text-left text-dark h6"><i class="bi bi-plus-circle-fill"></i> Adicionar Bloco</a>`
        $('#renderBloco').html('');
        
        $.template("loadBloco", tempBloco)

        $.ajax({
            url : 'http://localhost/projetoyii-leticia/web/index.php?r=api/blocos/get-all',
            method: 'GET',
            dataType: 'json',
            success: function(data){
                if (data.resultSet) {
                    $.tmpl("loadBloco", data.resultSet).append("#renderBloco")
                }
                $('.loader').remove();
                $('#linkAdd').html(link);
            }
        })
    }

    renderBloco();

    // Abrir edição de registro
    $(document).on('click', 'a[href="#"].editar', function(){
        idRegistro = $(this).attr('data-id');
        link = $(this);
        $.ajax({
            url : 'http://localhost/projetoyii-leticia/web/index.php?r=api/blocos/get-one&id='+idRegistro,
            method: 'GET',
            dataType: 'json',
            success: function(data){
                condominio = data.resultSet[0].nomeCondominio;
                from_condominio = data.resultSet[0].from_condominio;
                loadCond(from_condominio);
                $(link).parent().parent().find('.from_condominio').html(`<select name="from_condominio" class="fromCond"></select>`);
                
                nomeBloco = data.resultSet[0].nomeBloco;
                $(link).parent().parent().find('.nomeBloco').html(`<input type="text" name="nomeBloco" value="${nomeBloco}">`);
                andares = data.resultSet[0].andares;
                $(link).parent().parent().find('.andares').html(`<input type="text" name="andares" value="${andares}">`);
                qtdUnidades = data.resultSet[0].qtdUnidades;
                $(link).parent().parent().find('.qtdUnidades').html(`<input type="text" name="qtdUnidades" value="${qtdUnidades}">`);
            }
        })
        // Mudar botões de editar/remover para salvar/cancelar
        $(link).parent().find('.editar').toggleClass(['editar', 'salvar']);
        $(link).html('<i class="bi bi-check-circle-fill text-dark h4"></i>');
        
        $(link).parent().find('.remover').html('<i class="bi bi-x-circle text-dark h4"></i>');
        $(link).parent().find('.remover').toggleClass(['remover', 'cancelar']);

        $('.editar, .remover').removeAttr('href');
    })
    
    // Cancelar edição
    $(document).on('click', '.cancelar', function(){
        $(this).parent().parent().find('.from_condominio').html(condominio);
        $(this).parent().parent().find('.nomeBloco').html(nomeBloco);
        $(this).parent().parent().find('.andares').html(andares);
        $(this).parent().parent().find('.qtdUnidades').html(qtdUnidades);
    
        // Criar botões
        $(this).parent().find('.salvar').html('<i class="bi bi-pencil-square text-dark h4"></i>');
        $(this).parent().find('.salvar').toggleClass(['salvar', 'editar']);
    
        $(this).parent().find('.cancelar').html('<i class="bi bi-trash3 text-dark h4"></i>');
        $(this).parent().find('.cancelar').toggleClass(['cancelar', 'remover']);

        $('.editar, .remover').attr('href','#');
    })


    // Editar registro
    $(document).on('click','.salvar',function(){
        
        id = $(this).attr('data-id')
        from_condominio = $('select[name="from_condominio"]').val();
        nomeBloco = $('input[name="nomeBloco"]').val();
        andares = $('input[name="andares"]').val();
        qtdUnidades = $('input[name="qtdUnidades"]').val();

        dados = `from_condominio=${from_condominio}&nomeBloco=${nomeBloco}&andares=${andares}&qtdUnidades=${qtdUnidades}&id=${id}`;

        if (from_condominio, nomeBloco, andares, qtdUnidades) {
            retorno = postApi('http://localhost/projetoyii-leticia/web/index.php?r=api/blocos/edit-bloco', dados);
        }
    })


    // Deletar Registro
    $(document).on('click', '.remover', function(){
        idRegistro = $(this).attr('data-id');
        postApi('http://localhost/projetoyii-leticia/web/index.php?r=api/blocos/delete', `&id=${idRegistro}`);
    })
})