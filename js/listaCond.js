$(function(){

    // Listagem
    function renderCond(){
        $('#linkAdd').html('');
        link = `<a href="?page=cadCond" class="text-left text-dark h6"><i class="bi bi-plus-circle-fill"></i> Adicionar Condomínio</a>`
        $('#renderCond').html('');

        $.template("loadCond", tempCondo);

        $.ajax({
            url : 'http://localhost/projetoyii-leticia/web/index.php?r=api/condominios/get-all',
            method: 'GET',
            dataType: 'json',
            success: function(data){
                if (data.resultSet) {
                   $.tmlp("loadCond", data.resultSet).append("#renderCond");
                }
                $('.loader').remove();
                $('#linkAdd').html(link);
            }
        })
    }

    renderCond();

    // Abrir edição de registro
    $(document).on('click', 'a[href="#"].editar', function(){
        idRegistro = $(this).attr('data-id');
        link = $(this);
        $.ajax({
            url : 'http://localhost/projetoyii-leticia/web/index.php?r=api/condominios/get-one&id='+idRegistro,
            method: 'GET',
            dataType: 'json',
            success: function(data){
                nomeAdm = data.resultSet[0].nomeAdm;
                from_administradora = data.resultSet[0].from_administradora;
                loadAdm(from_administradora)
                $(link).parent().parent().find('.from_administradora').html(`<select name="from_administradora" class="fromAdm"></select>`);

                nomeCondominio = data.resultSet[0].nomeCondominio;
                $(link).parent().parent().find('.nomeCondominio').html(`<input type="text" name="nomeCondominio" value="${nomeCondominio}">`);
                qtdBlocos = data.resultSet[0].qtdBlocos;
                $(link).parent().parent().find('.qtdBlocos').html(`<input type="text" name="qtdBlocos" value="${qtdBlocos}">`);
                lougradouro = data.resultSet[0].lougradouro;
                $(link).parent().parent().find('.lougradouro').html(`<input type="text" name="lougradouro" value="${lougradouro}">`);
                numero = data.resultSet[0].numero;
                $(link).parent().parent().find('.numero').html(`<input type="text" name="numero" value="${numero}">`);
                bairro = data.resultSet[0].bairro;
                $(link).parent().parent().find('.bairro').html(`<input type="text" name="bairro" value="${bairro}">`);
                cidade = data.resultSet[0].cidade;
                $(link).parent().parent().find('.cidade').html(`<input type="text" name="cidade" value="${cidade}">`);
                estado = data.resultSet[0].estado;
                $(link).parent().parent().find('.estado').html(`<input type="text" name="estado" value="${estado}">`);
                cep = data.resultSet[0].cep;
                $(link).parent().parent().find('.cep').html(`<input type="text" name="cep" value="${cep}">`);
                sindico = data.resultSet[0].sindico;
                $(link).parent().parent().find('.sindico').html(`<input type="text" name="sindico" value="${sindico}">`);

            }
        })
        // Mudar botões de editar/remover para salvar/cancelar
        $(link).parent().find('.editar').toggleClass(['editar', 'salvar']);
        $(link).html('<i class="bi bi-check-circle-fill text-dark h4"></i>');
        
        $(link).parent().find('.remover').html('<i class="bi bi-x-circle text-dark h4"></i>');
        $(link).parent().find('.remover').toggleClass(['remover', 'cancelar']);

        $('.editar, .remover').removeAttr('href');
    })

    // Cancelar edição
    $(document).on('click', '.cancelar', function(){
        $(this).parent().parent().find('.from_administradora').html(nomeAdm);
        $(this).parent().parent().find('.nomeCondominio').html(nomeCondominio);
        $(this).parent().parent().find('.qtdBlocos').html(qtdBlocos);
        $(this).parent().parent().find('.lougradouro').html(lougradouro);
        $(this).parent().parent().find('.numero').html(numero);
        $(this).parent().parent().find('.bairro').html(bairro);
        $(this).parent().parent().find('.cidade').html(cidade);
        $(this).parent().parent().find('.estado').html(estado);
        $(this).parent().parent().find('.cep').html(cep);
        $(this).parent().parent().find('.sindico').html(sindico);

        // Criar botões
        $(this).parent().find('.salvar').html('<i class="bi bi-pencil-square text-dark h4"></i>');
        $(this).parent().find('.salvar').toggleClass(['salvar', 'editar']);

        $(this).parent().find('.cancelar').html('<i class="bi bi-trash3 text-dark h4"></i>');
        $(this).parent().find('.cancelar').toggleClass(['cancelar', 'remover']);

        $('.editar, .remover').attr('href','#');
    })


    // Editar registro
    $(document).on('click','.salvar',function(){
        
        id = $(this).attr('data-id')
        from_administradora = $('select[name="from_administradora"]').val();
        nomeCondominio = $('input[name="nomeCondominio"]').val();
        qtdBlocos = $('input[name="qtdBlocos"]').val();
        lougradouro = $('input[name="lougradouro"]').val();
        numero = $('input[name="numero"]').val();
        bairro = $('input[name="bairro"]').val();
        cidade = $('input[name="cidade"]').val();
        estado = $('input[name="estado"]').val();
        cep = $('input[name="cep"]').val();
        sindico = $('input[name="sindico"]').val();

        dados = `from_administradora=${from_administradora}&nomeCondominio=${nomeCondominio}&qtdBlocos=${qtdBlocos}&lougradouro=${lougradouro}&numero=${numero}&bairro=${bairro}&cidade=${cidade}&estado=${estado}&cep=${cep}&sindico=${sindico}&id=${id}`;

        if (from_administradora, nomeCondominio, qtdBlocos, lougradouro, numero, bairro, cidade, estado, cep, sindico) {
            retorno = postApi('http://localhost/projetoyii-leticia/web/index.php?r=api/condominios/edit-cond', dados);
        }
    })


    // Deletar Registro
    $(document).on('click', '.remover', function(){
        idRegistro = $(this).attr('data-id');
        postApi('http://localhost/projetoyii-leticia/web/index.php?r=api/condominios/delete', `&id=${idRegistro}`);
    })
})