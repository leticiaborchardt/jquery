<?error_reporting(0);
include 'includes/uteis.php'
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <title>JQuery</title>
</head>
<body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="?page=inicio">HOME</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="cad" role="button" data-toggle="dropdown" aria-expanded="false">Cadastros</a>
                    <div class="dropdown-menu" aria-labelledby="cad">
                        <a class="dropdown-item" href="?page=cadAdm">Cadastrar Administradora</a>
                        <a class="dropdown-item" href="?page=cadCond">Cadastrar Condomínio</a>
                        <a class="dropdown-item" href="?page=cadBloco">Cadastrar Bloco</a>
                        <a class="dropdown-item" href="?page=cadUni">Cadastrar Unidade</a>
                        <a class="dropdown-item" href="?page=cadMor">Cadastrar Morador</a>
                        <a class="dropdown-item" href="?page=cadConse">Cadastrar Conselho</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="list" role="button" data-toggle="dropdown" aria-expanded="false">Listagem</a>
                    <div class="dropdown-menu" aria-labelledby="list">
                        <a class="dropdown-item" href="?page=listaAdm">Listar Administradoras</a>
                        <a class="dropdown-item" href="?page=listaCond">Listar Condomínios</a>
                        <a class="dropdown-item" href="?page=listaBloco">Listar Blocos</a>
                        <a class="dropdown-item" href="?page=listaUni">Listar Unidades</a>
                        <a class="dropdown-item" href="?page=listaMor">Listar Moradores</a>
                        <a class="dropdown-item" href="?page=listaConse">Listar Conselhos</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>

    <main class="container mb-5">
        <?
        switch ($_GET['page']) {
            case '':
            case 'inicio':
                require "views/inicio.php";
                break;
            default:
                require 'views/'.$_GET['page'] . '.php';
                break;
        }
        ?>
    </main>

    <footer class="fixed-bottom">
        <div class="w-100 py-2 px-2 bg-light text-dark tx-small"><small>&copy; Todos os direitos reservados.</small></div>
    </footer>

    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
    <script src="js/jquery.tmpl.min.js"></script>
    <script src="js/templates.js"></script>
    <script src="js/app.js"></script>
    <script src="js/<?=$_GET['page']?>.js"></script>
</body>
</html>