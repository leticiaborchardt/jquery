<h1 class="my-4 h3 text-center">Cadastrar Conselho</h1>
<form action="http://localhost/projetoyii-leticia/web/index.php?r=api/conselhos/register-conse">
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="condominioBloco">A qual condomínio este conselho pertence?</label>
            <select class="form-control shadow mb-3 bg-white rounded fromCond" name="from_condominio" required></select>
        </div> 
    </div>
    <div class="form-row">
        <div class="form-group col-md-8">
            <label for="nomeSindico">Nome Completo</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nome" required>
        </div>
        <div class="form-group col-md-4">
            <label for="nomeSindico">Função</label>
            <select class="form-control shadow mb-3 bg-white rounded" name="from_funcao" required>
                <option value="" disabled selected>Selecione a Função</option>
                <option value="Subsíndico">Subsíndico(a)</option>
                <option value="Conselheiro">Conselheiro(a)</option>
            </select>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-success mr-2">Cadastrar</button>
            <a class="btn btn-sm btn-secondary" href="?page=listaConse" role="button">Ir para a listagem</a>
        </div>
    </div>
</form>