<h1 class="my-4 h3 text-center">Cadastrar Condomínio</h1>
<form action="http://localhost/projetoyii-leticia/web/index.php?r=api/condominios/register-cond">
    <div class="form-row mt-4">
        <div class="form-group col-md-12">
            <label for="from_administradora">Administradora</label>
            <select class="form-control shadow mb-3 bg-white rounded fromAdm" name="from_administradora" required></select>
        </div>
    </div>

    <div class="form-row mt-4">
        <div class="form-group col-md-9">
            <label for="nomeCondominio">Nome do condomínio</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nomeCondominio" required>
        </div>
        <div class="form-group col-md-3">
            <label for="qtdBlocos">Quantidade de Blocos</label>
            <input type="number" min="1" class="form-control shadow mb-3 bg-white rounded" name="qtdBlocos" required>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-12">
            <h5>Endereço</h5>
        </div>
        <div class="form-group col-md-3">
            <label for="estado">Estado</label>
            <select class="form-control shadow mb-3 bg-white rounded fromEstado" name="estado">
                <option value="" disabled selected>Selecione</option>
                <?
                foreach ($estadosAPI as $sig => $uf) {?>
                    <option data-uf="<?=$sig?>" value="<?=$uf?>"><?=$uf?></option>
                    <? } ?>
                </select>
        </div> 
        <div class="form-group col-md-6">
            <label for="cidade">Cidade</label>
            <select class="form-control shadow mb-3 bg-white rounded cidades" name="cidade"></select>
        </div>
        <div class="form-group col-md-3">
            <label for="cep">CEP</label>
            <input type="text" maxlength="8" class="form-control shadow mb-3 bg-white rounded" name="cep">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-7">
            <label for="lougradouro">Lougradouro</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="lougradouro">
        </div>
        <div class="form-group col-md-2">
            <label for="numero">Número</label>
            <input type="number" min="1" class="form-control shadow mb-3 bg-white rounded" name="numero">
        </div>
        <div class="form-group col-md-3">
            <label for="bairro">Bairro</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="bairro">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-12">
            <h5>Síndico</h5>
        </div>
        <div class="form-group col-md-12">
            <label for="sindico">Nome completo do síndico(a)</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="sindico" required>
        </div>
    </div>

    <div class="row mb-5">
        <div class="col-12 mb-4">
            <button type="submit" class="btn btn-success mr-2">Cadastrar</button>
            <a class="btn btn-sm btn-secondary" href="?page=listaCond" role="button">Ir para a listagem</a>
        </div>
    </div>
</form>
