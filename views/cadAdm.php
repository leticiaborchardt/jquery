<h1 class="my-4 h3 text-center">Cadastrar Administradora</h1>
<form action="http://localhost/projetoyii-leticia/web/index.php?r=api/administradoras/register-adm">
    <div class="form-row">
        <div class="form-group col-md-8">
            <label for="nome">Nome da Administradora</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nome" required>
        </div>
        <div class="form-group col-md-4">
            <label for="cnpj">CNPJ</label>
            <input type="text" maxlength="14" class="form-control shadow mb-3 bg-white rounded" name="cnpj" required>
        </div>
    </div>
    
    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-success mr-2">Cadastrar</button>
            <a class="btn btn-sm btn-secondary" href="?page=listaAdm" role="button">Ir para a listagem</a>
        </div>
    </div>
</form>