<h1 class="my-4 h3 text-center">Cadastrar Unidade</h1>
<form action="http://localhost/projetoyii-leticia/web/index.php?r=api/unidades/register-uni">
    <div class="form-row">
        <div class="form-group col-md-8">
            <label for="condominioBloco">A qual condomínio esta unidade pertence?</label>
            <select class="form-control shadow mb-3 bg-white rounded fromCond" name="from_condominio" required></select>
        </div>
        <div class="form-group col-md-4">
            <label for="condominioBloco">A qual bloco esta unidade pertence?</label>
            <select class="form-control shadow mb-3 bg-white rounded fromBloco" name="from_bloco" required></select>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="nomeUnidade">Nomenclatura da Unidade</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nomeUnidade" placeholder="Ex.: Apto 101"required>
        </div>
        <div class="form-group col-md-4">
            <label for="metragem">Metragem (m²)</label>
            <input type="number" min="1" class="form-control shadow mb-3 bg-white rounded" name="metragem" required>
        </div>
        <div class="form-group col-md-4">
            <label for="qtdGaragem">Quantidade de Garagens</label>
            <input type="number" min="0" class="form-control shadow mb-3 bg-white rounded" name="qtdGaragem" required>
        </div>
    </div>
    
    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-success mr-2">Cadastrar</button>
            <a class="btn btn-sm btn-secondary" href="?page=listaUni" role="button">Ir para a listagem</a>
        </div>
    </div>
</form>