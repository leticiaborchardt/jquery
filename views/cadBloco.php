<h1 class="my-4 h3 text-center">Cadastrar Bloco</h1>
<form action="http://localhost/projetoyii-leticia/web/index.php?r=api/blocos/register-bloco">
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="condominioBloco">A qual condomínio este bloco pertence?</label>
            <select class="form-control shadow mb-3 bg-white rounded fromCond" name="from_condominio" required></select>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="nomeBloco">Nomenclatura do Bloco</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nomeBloco" placeholder="Ex.: Bloco A / Bloco 01..."required>
        </div>
        <div class="form-group col-md-4">
            <label for="andares">Quantidade de Andares</label>
            <input type="number" min="1" class="form-control shadow mb-3 bg-white rounded" name="andares" required>
        </div>
        <div class="form-group col-md-4">
            <label for="qtdUnidades">Quantidade de Unidades por andar</label>
            <input type="number" min="1" class="form-control shadow mb-3 bg-white rounded" name="qtdUnidades" required>
        </div>
    </div>
    
    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-success mr-2">Cadastrar</button>
            <a class="btn btn-sm btn-secondary" href="?page=listaBloco" role="button">Ir para a listagem</a>
        </div>
    </div>
</form>