<h1 class="my-4 h3 text-center">Administradoras cadastradas</h1>
<div class="table-responsive">
    <table id="renderAdm" class="table table-striped table-hover shadow bg-white rounded">
        <tr>
            <th>Nome</th>
            <th>CNPJ</th>
            <th>Data Cadastro</th>
            <th>Ações</th>
        </tr>
    </table>  
</div>

<div class="col-12 col-md-4" id="linkAdd">

</div>

<div class="col-12 text-center loader">
    <div class="spinner-border loader" role="status">
    <span class="sr-only">Loading...</span>
    </div>
</div>