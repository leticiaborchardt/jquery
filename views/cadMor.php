<h1 class="my-4 h3 text-center">Cadastrar Morador</h1>
<form action="http://localhost/projetoyii-leticia/web/index.php?r=api/moradores/register-mor">
    <div class="form-row">
        <div class="form-group col-md-4">
        <label for="nomeCondominio">Condomínio</label>
            <select class="form-control shadow mb-3 bg-white rounded fromCond" name="from_condominio" required></select>
        </div>
        <div class="form-group col-md-4">
        <label for="nomeBloco">Bloco</label>
            <select class="form-control shadow mb-3 bg-white rounded fromBloco" name="from_bloco" required></select>
        </div>
        <div class="form-group col-md-4">
        <label for="nomeUnidade">Unidade</label>
            <select class="form-control shadow mb-3 bg-white rounded fromUni" name="from_unidade" required></select>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="nome">Nome completo</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nome" required>
        </div>
        <div class="form-group col-md-6">
            <label for="cpf">CPF/CNPJ</label>
            <input type="text" maxlength="11" class="form-control shadow mb-3 bg-white rounded" name="cpf" required>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-8">
            <label for="email">E-mail</label>
            <input type="email" class="form-control shadow mb-3 bg-white rounded" name="email" required>
        </div>
        <div class="form-group col-md-4">
            <label for="tel">Telefone/Celular</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="telefone">
        </div>
    </div>
    
    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-success mr-2">Cadastrar</button>
            <a class="btn btn-sm btn-secondary" href="?page=listaMor" role="button">Ir para a listagem</a>
        </div>
    </div>
</form>