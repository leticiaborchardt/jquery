<h1 class="my-4 h3 text-center">Blocos cadastrados</h1>
<div class="table-responsive">
    <table id="renderBloco" class="table table-striped table-hover shadow bg-white rounded">
        <tr>
            <th>Condomínio</th>
            <th>Bloco</th>
            <th>Qtd. Andares</th>
            <th>Qtd. Unidades</th>
            <th>Data Cadastro</th>
            <th>Ações</th>
        </tr>
    </table>
</div>

<div class="col-12 col-md-4" id="linkAdd">

</div>

<div class="col-12 text-center loader">
    <div class="spinner-border loader" role="status">
    <span class="sr-only">Loading...</span>
    </div>
</div>